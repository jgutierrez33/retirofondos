/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.retirofondos;

import com.mycompany.retirofondos.entety.Inscripcionretiro;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.ok;

/**
 *
 * @author WSP
 */

@Path("/Inscripcion")
public class Inscripcion {
    
  EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
 EntityManager em;  
 
 
 @GET
 @Produces(MediaType.APPLICATION_JSON)
 public Response listartodo(){
       em = emf.createEntityManager();
       List<Inscripcionretiro> lista = em.createNamedQuery("Inscripcionretiro.findAll").getResultList();
        return Response.ok(200).entity(lista).build();
 
 }
 
 @GET
 @Path("/{idbuscar}")
 @Produces(MediaType.APPLICATION_JSON)
 
  public Response buscarId(@PathParam("idbuscar") String idbuscar) {
   
         em = emf.createEntityManager();
         Inscripcionretiro inscripcion = em.find(Inscripcionretiro.class, idbuscar);
          return Response.ok(200).entity(inscripcion).build();
}
  
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  public String nuevo (Inscripcionretiro Nuevainscripcion){
      em = emf.createEntityManager();
      em.getTransaction().begin();
      em.persist(Nuevainscripcion);
      em.getTransaction().commit();
      return ("cliente guardado");
      
  }
  
  @PUT
  public Response actualizar (Inscripcionretiro clienteactualizar){
      String resultado = null;
      
      em = emf.createEntityManager(); 
      em.getTransaction().begin();
      clienteactualizar = em.merge(clienteactualizar);
      em.getTransaction().commit();
      return Response.ok(clienteactualizar).build();
      
      
  }
  
  @DELETE
  @Path("/{idborrar}")
  @Produces ({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
  public Response eliminar(@PathParam("idborrar")String idborrar){
     em = emf.createEntityManager();      
     em.getTransaction().begin();
     Inscripcionretiro  clieliminar  = em.getReference(Inscripcionretiro.class, idborrar);
     em.remove(clieliminar);
     em.getTransaction().commit();
    return Response.ok("cliente eliminado").build();
  
 }
}