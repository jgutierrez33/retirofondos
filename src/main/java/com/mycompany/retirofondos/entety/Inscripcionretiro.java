/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.retirofondos.entety;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gaby
 */
@Entity
@Table(name = "inscripcionretiro")
@NamedQueries({
    @NamedQuery(name = "Inscripcionretiro.findAll", query = "SELECT i FROM Inscripcionretiro i"),
    @NamedQuery(name = "Inscripcionretiro.findByRut", query = "SELECT i FROM Inscripcionretiro i WHERE i.rut = :rut"),
    @NamedQuery(name = "Inscripcionretiro.findByPorcentaje", query = "SELECT i FROM Inscripcionretiro i WHERE i.porcentaje = :porcentaje")})
public class Inscripcionretiro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "porcentaje")
    private String porcentaje;

    public Inscripcionretiro() {
    }

    public Inscripcionretiro(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inscripcionretiro)) {
            return false;
        }
        Inscripcionretiro other = (Inscripcionretiro) object;
        return !((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut)));
    }

    @Override
    public String toString() {
        return "com.mycompany.retirofondos.entety.Inscripcionretiro[ rut=" + rut + " ]";
    }

    public String getr() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
